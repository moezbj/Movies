import React from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import { Images } from "../Theme";
import "../styles/Navbar.css";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="navbar">
        <div className="shop">
          <p>{this.props.count}</p>
          <Link to="/shop">
            <img src={Images.shopping} />
          </Link>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  const count = state.films.count;
  return {
    count
  };
};

export default connect(mapStateToProps, null)(HomePage);
