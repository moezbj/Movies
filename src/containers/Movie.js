import React from "react";
import { connect } from "react-redux";
import "../styles/movie.css";
import { addMovie } from "../redux/actions/MoviesAction";
class Movie extends React.Component {
  render() {
    const { movies } = this.props;
    return (
      <div className="movie-card">
        {movies.map((el, i) => (
          <div className="movie-Container" key={i}>
            <div className="movie-title">title:{el.title}</div>
            <img className="movie-img" src={el.src} alt="logo" />
            <div>price:{el.price}$</div>
            <button
              className="movie-add"
              onClick={() => this.props.addMovie(el)}
            >
              Add Movie
            </button>
          </div>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => {
  const movies = state.films.movies;
  return {
    movies
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addMovie: movie => {
      dispatch(addMovie(movie));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Movie);
