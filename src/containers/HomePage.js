import React from "react";
import { connect } from "react-redux";
import Movie from "./Movie";
import NavBar from "./NavBar";
import ShopPage from "./ShopPage";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="app">
        <NavBar />
        <Movie />
      </div>
    );
  }
}
const mapStateToProps = ({}) => ({});

export default connect(mapStateToProps, null)(HomePage);
