import React from "react";
import { connect } from "react-redux";
import "../styles/Shop.css";
import { removeMovie } from "../redux/actions/MoviesAction";
class ShopePage extends React.Component {
  render() {
    const { myCart } = this.props;
    return (
      <div className="cart">
        <div>total:{this.props.total}</div>
        {myCart.map((el, index) => (
          <div className="movie-Container" key={index}>
            <div className="movie-title">title:{el.title}</div>
            <img className="movie-img" src={el.src} alt="logo" />
            <div>price:{el.price}$</div>
            <button
              className="movie-add"
              onClick={() => this.props.removeMovie({ ...el, index })}
            >
              delete Movie
            </button>
          </div>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => {
  const myCart = state.films.myCart;
  const total = state.films.total;
  return {
    myCart,
    total
  };
};
const mapDispatchToProps = dispatch => {
  return {
    removeMovie: movie => {
      dispatch(removeMovie(movie));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShopePage);
