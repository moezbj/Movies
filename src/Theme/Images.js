const images = {
  superman: require("../images/super-man.png"),
  justiceLeague: require("../images/jl.png"),
  avatar: require("../images/avatar.png"),
  pirate: require("../images/pirate.png"),
  fallout: require("../images/fallout.png"),
  shopping: require("../images/shopping-cart.png")
};
export default images;
