import React from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import HomePage from "../containers/HomePage";
import ShopPage from "../containers/ShopPage";
class Routes extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={HomePage} />
          <Route path="/shop" component={ShopPage} />
        </div>
      </Router>
    );
  }
}

export default Routes;
