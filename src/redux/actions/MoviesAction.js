import { ADD_MOVIE, REMOVE_MOVIE, REMOVE_ALL } from "./types";

export function addMovie(movie) {
  return {
    type: ADD_MOVIE,
    movie
  };
}

export function removeMovie(movie) {
  return {
    type: REMOVE_MOVIE,
    movie
  };
}
export function removeAll(movie) {
  return {
    type: REMOVE_ALL,
    movie
  };
}
