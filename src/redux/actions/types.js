export const ADD_MOVIE = "add_movie";
export const REMOVE_MOVIE = "remove_movie";
export const UPDATE_MOVIE = "update_movie";
export const REMOVE_ALL = "remove_all";
