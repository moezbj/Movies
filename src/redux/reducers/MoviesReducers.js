import { ADD_MOVIE, REMOVE_MOVIE, REMOVE_ALL } from "../actions/types";
import { Images } from "../../Theme";

const INITIAL_STATE = {
  movies: [
    {
      title: "Pirate",
      src: Images.pirate,
      price: 18
    },
    {
      title: "Superman",
      src: Images.superman,
      price: 14
    },
    {
      title: "Justice League",
      src: Images.justiceLeague,
      price: 12
    },
    {
      title: "avatar",
      src: Images.avatar,
      price: 30
    },
    {
      title: "fallout",
      src: Images.fallout,
      price: 18
    }
  ],
  myCart: [],
  total: 0,
  count: 0
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_MOVIE:
      const myCart = state.myCart;
      myCart.push({
        title: action.movie.title,
        src: action.movie.src,
        price: action.movie.price
      });
      return {
        ...state,
        myCart,
        total: state.total + action.movie.price,
        count: state.count + 1
      };
    case REMOVE_MOVIE:
      console.log(action);
      const cart = state.myCart;
      const { price, index } = action.movie;
      cart.splice(index, 1);
      return {
        ...state,
        cart,
        total: state.total - price,
        count: state.count - 1
      };
    case REMOVE_ALL:
      return { ...state, myCart: [], total: 0, count: 0 };
    default:
      return state;
  }
};
