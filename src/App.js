import React, { Component } from "react";

import { Provider } from "react-redux";
import { createStore } from "redux";

import logo from "./logo.svg";
import "./styles/App.css";

import reducers from "./redux/reducers";
import HomePage from "./containers/HomePage";

import Routes from "./routes/Routes";
class App extends Component {
  render() {
    const store = createStore(reducers);

    return (
      <Provider store={store}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">
              Welcome to React application with redux{" "}
            </h1>
          </header>
          <Routes />
        </div>
      </Provider>
    );
  }
}

export default App;
