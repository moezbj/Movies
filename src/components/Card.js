import React from "react";
import { connect } from "react-redux";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="card">
        <p>{this.title}</p>
        <div className="app-body"> </div>
        <p />
      </div>
    );
  }
}
const mapStateToProps = ({}) => ({});

export default connect(mapStateToProps, null)(HomePage);
